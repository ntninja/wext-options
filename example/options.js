// Some helper code for integrating more nicely with `wext-options`
const OPTION_HOOKS = {
	"color": function(option, api) {
		function onChange(event) {
			if(event.value === "custom") {
				document.getElementById("option_color_custom").disabled = false;
			} else {
				document.getElementById("option_color_custom").disabled = true;
			}
		}
		option.onUserChange.addListener(onChange);
		option.onStorageChange.addListener(onChange);
		
		api.onReady.addListener((event) => {
			onChange({ value: option.value });
		});
	}
};
