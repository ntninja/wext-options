# Useage example

You can use the example in this directory as a template for creating new
`wext-options` based extensions.

To try out the example files create a new directory and run:

```
# Create a new GIT repository inside the directory
git init

# Fetch a copy of `wext-options`
git submodule add https://gitlab.com/alexander255/wext-options.git deps/wext-options

# Copy the example files to an appropriate location
cp -r deps/wext-options/example content

# Create extension manifest
echo '{
	"manifest_version": 2,
	
	"name":    "WebExtensions options panel example",
	"version": "1.0",
	
	"permissions": [
		"storage"
	],
	
	"options_ui": {
		"page": "content/options.html",
		"browser_style": true
	}
}' > manifest.json

# Run WebExtension
web-ext run
```

Once the browser has opened simply navigate to the *Add-Ons* panel, click *Preferences* and enjoy the result:

![Example Screenshot](../screenshots/example.png)
